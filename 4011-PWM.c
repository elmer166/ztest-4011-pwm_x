/* \file 4011-PWM.c
 *
 * \brief PWM demo on dsPIC30F4011 "dsPIC-EL" board
 *
 *
 * Author: jjmcd
 *
 * Created on September 2, 2012, 10:03 AM
 */

/******************************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2012 by John J. McDonough, WB8RCR
 *
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 *
 *****************************************************************************/

#include <xc.h>
#include <math.h>

// Configuration fuses
_FOSC( XT_PLL16 & PRI )                     // 7.3728 rock * 16 = 118MHz
_FWDT( WDT_OFF )                            // Watchdog timer off
_FBORPOR( PWRT_16 & PBOR_OFF & MCLR_EN )    // Brownout off, powerup 16ms
_FGS( GWRP_OFF & CODE_PROT_OFF )            // No code protection

// Manifest constants
#define SLOWITDOWN 50000
#define PI 3.14159268
#define TWOPI 2.0*PI
#define PIOVER4 PI/4.0

//	Function Prototypes
int main (void);


//! main - Vary the brightness of the LEDs depending on the sine of an angle

/*! Timer 2 is used for the PWM outputs.  The 3 PWMs are all set up
 * identically.  In the mainline, we loop through 360 degrees, and scale
 * the sine of the angle from -1.0 to +1.0 to 0 to 1024, the PWM range.
 * One LED leads by 90 degrees, the other trails by 90 degrees.
 */
int main (void)
{
    long i;
    float theta;

    // Make PORTD outputs
    TRISD = 0;

    // Set up timer 2 for PWM
    TMR2 = 0;               // Clear timer 2
    PR2 = 1000;             // Timer 2 counter to 1000
    T2CON = 0x8010;         // Fosc/4, 1:4 prescale, start TMR2

    // Set up PWM on OC2 (RD1)
    OC2RS = 1024;           // PWM 2 duty cycle
    OC2R = 0;               //
    OC2CON = 0x6;           // Set OC2 to PWM mode, timer 2

    // Set up PWM on OC3 (RD2)
    OC3RS = 1024;           // PWM 3 duty cycle
    OC3R = 0;               //
    OC3CON = 0x6;           // Set OC4 to PWM mode, timer 2

    // Set up PWM on OC4 (RD3)
    OC4RS = 1024;           // PWM 4 duty cycle
    OC4R = 0;               //
    OC4CON = 0x6;           // Set OC4 to PWM mode, timer 2

  while (1)
    {
      // Loop through 360 degrees
      for ( theta=0.0; theta<TWOPI; theta += 0.05 )
        {
            // Set the brightness of each LED based on the sine
            // of the angle.  The order here looks a little odd
            // because the position of the LEDs on the board differs
            // from the order of the port numbers.  The red LED is
            // cycled through the full scale, but the others are never
            // 100% on but scaled back for similar  brightness
            OC3RS = (int)(768.0-256.0*sin(theta-PIOVER4));
            OC4RS = (int)(896.0-128.0*sin(theta));
            OC2RS = (int)(512.0-512.0*sin(theta+PIOVER4));
            // Slow it down
            for ( i=0; i<SLOWITDOWN; i++ )
                ;
        }
    }
}



